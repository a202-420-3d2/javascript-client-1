const express = require('express');
const knexModule = require('knex');

const app = express();
const port = 3000;

app.use(express.static('public'));

const knex = knexModule({
  client: 'mysql',
  connection: {
    host: '3d2.cm9.ca',
    user: 'reader',
    password: 'Tomate123',
    database: 'cars',
    port: 3306,
  },
});

function htmlTemplate(mainPart) {
  return `<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>Liste des automobiles</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    </head>
    <p>
      <div class="container">
        ${mainPart}
      </div>
    </body>
    </html>`;
}

app.get('/', async (req, res) => {
  const carMakes = await knex('cars')
    .distinct('car_make')
    .orderBy('car_make')
    .pluck('car_make');

  const carMakeOptions = carMakes.map((text) => `<option value="${text}">${text}</option>`);

  const html = `<h1 class="title">Liste des marques</h1>
    <p>
      Choisir une des marques suivantes: 
      <form action="/list-make">
        <div class="select">
          <select name="make">
            <option></option>
            ${carMakeOptions}
          </select>                    
        </div>
      </form>
    </p>`;

  res.send(htmlTemplate(html));
});

app.get('/list-make', async (req, res) => {
  const param = req.query.make;

  const data = await knex('cars')
    .count('*', { as: 'total' })
    .select('car_model')
    .where('car_make', param)
    .groupBy('car_model')
    .orderBy('car_model');

  const tableRows = data.map((row) => `<tr>
    <td>${row.car_model}</td>
    <td>${row.total}</td>
  </tr>`);

  const html = `<h1 class="title">${param}</h1>
    <table class="table">
      <thead>
        <tr>
          <th>Modèle</th>
          <th>Nombre de propriétaires</th>
        </tr>
      </thead>
      <tbody>
        ${tableRows.join('')}
      </tbody>
    </table>`;

  res.send(htmlTemplate(html));
});

app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});
